clear all;
close all;
format long;

tic
popwt = 1.00; % weight on population objective; weight on harvest obj is 1-popwt
goal = 60;

% Discretization of state and control variables
Nmin = 0; Nmax = 100; Ninc = 1;  % population size in May
Dmin = 0; Dmax = 30;  Dinc = 2;   % thaw days  
Hmin = 0; Hmax = 50;  Hinc = 1;   % harvest quotas (optimal harvests implicitly include crippling loss)

% Shocks
%... thaw days 2000 ->
e1 = (Dmin:Dinc:Dmax)';   
w1 = [0.0386 0.0825 0.1057 0.1158 0.1166 0.1110 0.1007 0.0876 0.0730 0.0581 0.0436 0.0306 0.0195 0.0108 0.0047 0.0012]';
%sum(w1)

%... theta (naturual survival)
[e2,w2] = qnwbeta(5, 51.364034,  6.841186 );
Etheta = e2'*w2;

% relation between preseason age ratio and thaw days
[e34,w34] = qnwnorm([5 5],[-1.882014  0.495734],[0.007014538 -0.005579059; -0.005579059  0.005071359]);
Ebeta0 = w34'*e34(:,1); Ebeta1 = w34'*e34(:,2);

% diff vulnerability
[e5,w5] = qnwgamma(5, 1.73411318, 0.02134593, 1);
Edv = e5'*w5;

e = rectgrid(e1,e2,e34,e5);
grid = rectgrid(w1,w2,w34,w5);
w = (prod(grid,2));  
sum(w); 

% Define state and action vectors
N = (Nmin:Ninc:Nmax)'; 
D = (Dmin:Dinc:Dmax)';
S = rectgrid(N,D);
H = (Hmin:Hinc:Hmax)';
X = rectgrid(S,H);  % State,Action combination

% state transition functions
rfcn = @(D,beta0,beta1) (1./(1+exp(-(beta0+beta1.*D./10)))) ./ (1 - (1./(1+exp(-(beta0+beta1.*D./10)))));
PFtran = @(N,D,H,theta,beta0,beta1,dv) max(0,( ...
  N.*theta.*( ...
  ( 1-H./(N.*theta.^(4/12).*(1+dv.*rfcn(D,beta0,beta1)))) + ...
  rfcn(D,beta0,beta1).*(1-dv.* ...
      H./(N.*theta.^(4/12).*(1+dv.*rfcn(D,beta0,beta1)))) ...
   ) ...
));

Dtran = @(N,D,H) ones(size(N,1),1); % for every state/action return "1"; next year's days are given by 1 x e1, with probabilities w1

% compute transition matrices
pf = @(X,e) [max(0, PFtran(X(:,1),X(:,2),X(:,3),e(:,2),e(:,3),e(:,4),e(:,5))) Dtran(X(:,1),X(:,2),X(:,3)).*e(:,1)];
cleanup = 0; % 0 = none; 1 = neg values set to zero, renormalized; 2 = snap to grid
Stran = g2P(pf,{N,D},X,e,w,cleanup);

% objective function (constrain harvest <= N)
 ndx = (X(:,3)>X(:,1) & X(:,3)>0);
 Hutil = X(:,3)./max(H);
 Hutil(ndx) = -inf;
 
  EN = PFtran(X(:,1),X(:,2),X(:,3),Etheta,Ebeta0,Ebeta1,Edv); % expectation of next pop
  poputil = 1./(1+exp(-(10-(abs(EN-goal))))); % utility of next pop
%  poputil = 1./(1+exp(-(10-(abs(X(:,1)-goal))))); % utility of current pop (makes little difference)
  R = popwt*poputil + (1-popwt)*Hutil; 

% compute policy
I = getI(X,1:2);

% INFINITE TIME HORIZON, DELTA = 1; RELATIVE (AVERAGE) VALUE
% modelfull=struct('discount',1.0,'Ix',I);
% options=struct('algorithm','f','print',2,'tol',5e-6,'relval',1);

% INFINITE TIME HORIZON, DISCOUNTED; MODIFIED POLICY ITERATION
 modelfull=struct('d',0.99999,'Ix',I);
 options=struct('print',2);
 
% INFINITE TIME HORIZON VANISHING DISCOUNT
%   modelfull=struct('d',1.0,'Ix',I);
%   options=struct('algorithm','i','vanish',0.99999,'print',2);

modelfull.R = R;
modelfull.P = Stran;
results = mdpsolve(modelfull,options);

pstar = modelfull.P(:,results.Ixopt);
Sfreq=longrunP(pstar);
neg = Sfreq<0; i = find(neg); size(i);
Sfreq(i)=0; Sfreq = Sfreq/sum(Sfreq);
M=marginals(Sfreq,S);
Epop = M{1}'*N
SDpop = sqrt(var(N,M{1}))
Eh = Sfreq'*X(results.Ixopt,3)
SDh = sqrt(var(X(results.Ixopt,3),Sfreq))
Eval = Sfreq'*R(results.Ixopt)
SDval = sqrt(var(R(results.Ixopt),Sfreq))

policy=[X(results.Ixopt,:) results.v];
dlmwrite('policy_23May2022.csv', [policy Sfreq],'precision','%.15f');
minutes = toc/60

font="Arial";
x = N;
y = D;
z = reshape(policy(:,3),size(y,1),size(x,1));
figure('DefaultTextFontName', font, 'DefaultAxesFontName', font);
v=0:2.5:50;
contourf(y,x,z',v)
xlabel('D')
ylabel('N')
colormap(jet(21))
colorbar

save('policy_23May2022')


  
  